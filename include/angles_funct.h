/*
 * angles_funct.h
 *
 *  Created on: 9 gen 2020
 *      Author: ironmath
 *      Comment: angles_funct.h was designed in order to make the code more readeble
 */

#ifndef ANGLES_FUNCT_H_
#define ANGLES_FUNCT_H_

#include "config.h"

/* convert degree to radians */
double degToRad(double deg);

/* convert radians to degrees */
double radToDeg(double rad);

#endif /* ANGLES_FUNCT_H_ */
