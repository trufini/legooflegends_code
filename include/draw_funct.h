/*
 * draw_funct.h
 *
 *  Created on: 9 gen 2020
 *      Author: ironmath
 *      Comment: All these functions work on a char matrix, the name of the functions are self explicative,
 *      		 the library was designed used code, taken by different source, the biggest one is https://gist.github.com/bert/1085538
 *				 It is the only library where it is possible to see non static functions with "_" as starting character of the name,
 *				 that because of readability, i.e. it is simple to recognize that functions.
 *				 Information about the defines and some preprocessor directives are in the config.h file, because it was necessary
 *				 to tune some parameters and to speed up the work the tunable one are in that file.
 */

#ifndef DRAW_FUNCT_H_
#define DRAW_FUNCT_H_

#include "config.h"
#include "angles_funct.h"

//void _drawPoint(char field [X_DIM][Y_DIM],int x,int y,int typeOfField);

void _drawLine(char field [X_DIM][Y_DIM],int x1,int y1,int x2,int y2, int mode);

void _drawPolarLine(char field[X_DIM][Y_DIM],int x1,int y1,int rho,int thetaDeg, int mode);

void _drawTriangle(char field [X_DIM][Y_DIM], int x_fixed, int y_fixed ,int x1,int y1,int x2,int y2);

void _drawPolarTriangleColor(char field [X_DIM][Y_DIM],int mmToAdd, int x_fixed, int y_fixed ,int rho1,int thetaDeg1,int rho2,int thetaDeg2,int mode);

void _drawSquare(char field[X_DIM][Y_DIM], int x1, int y1 ,int x2,int y2, int mode);

void _filledCircle (char field[X_DIM][Y_DIM] ,int cx, int cy, int radius, int typeOfField);


#endif /* DRAW_FUNCT_H_ */
