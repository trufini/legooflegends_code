/*
 * debugFunction.h
 *
 *  Created on: 24 nov 2019
 *      Author: ironmath
 *      Comment: function designed with debugging purpose.
 */

#ifndef DEBUGFUNCTIONS_H_
#define DEBUGFUNCTIONS_H_


#define BUFFER_SIZE 1000


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "loc_funct.h"
#include "config.h"


/* Print the environment, i.e. the char matrix, in a file and add a special code to easily convert the matrix into an jpg image*/
void printEnvMap(char* path,fieldData  *pt_fData);

/*Load enviroment from a file*/
void loadEnvMap(char* path,fieldData  *pt_fData);

/*Load a test set of sonar data from a file with the following format
 * "%d,%d,", &angle, &sonarDistance */
int readTestDataFile(char *filenameOfSonarData);

/*Load in the real data struct the test data read from the file*/
void loadDataInStruct(sensorData* pt_sDataDebug);

/*Load in the real data struct the test data read from a matrix*/
void loadDataInStructMat(sensorData *pt_sDataDebug, float matrix[400][2]);

/* Modify the field adding some fake obj to test the algo*/
void fieldGenerator(fieldData  *pt_fData);


#endif /* DEBUGFUNCTIONS_H_ */
