/*
 * obj_detect.h
 *
 *  Created on: 15 dic 2019
 *      Author: ironmath
 *		Comment: obj_detect.h was designed with the objective to determine the shape one object through a vector
 *					containing vertical angles and distances. The algorithm is quite simple due to the lack of time
 *					between when the datas were available to me and the competition itself.
 *					It is a tree of check to determine:
 *					a) firstly if in the position where the scanning is done there is an obj; _isAnObject(verticalAng,verticalDistance);
 *					b) then it does a cascade of match:
 *						b.1) _isItACube(verticalAng,verticalDistance);
 *						b.2) _isItACilinder(verticalAng, verticalDistance);
 *						b.3) _isItAPiramid(verticalAng, verticalDistance);
 *					More details in the .c file.
 *
 */

#ifndef OBJ_DETECT_H_
#define OBJ_DETECT_H_

/* Code to be sent to the server */
#define UNIDENTIFIED 0
#define CUBE 		1
#define CILINDER 	6
#define PIRAMID4 	2
#define PIRAMID3 	3
#define PIRAMID4_UD	4
#define PIRAMID3_UD	5

#include <time.h>
#include <stdlib.h>
#include "loc_funct.h"
#include "config.h"


 /* Determine the shape of the obj calling few static function that process the datas looking for
  * known pattern */
int determineShape(float datas[400][2]);

/* Roughly extimation of possible position of obj */
void findObjPosition(fieldData *pt_fData, int num );


#endif /* OBJ_DETECT_H_ */
