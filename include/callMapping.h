/*
 * callMapping.h
 *
 *  Created on: 9 gen 2020
 *      Author: ironmath
 *      Comment: This function is the main of the mapping part.
 */

#ifndef CALLMAPPING_H_
#define CALLMAPPING_H_


#include <unistd.h>
#include <string.h>
#include "loc_funct.h"
#include "debugFunctions.h"
#include "obj_detect.h"







/* This function is the main of the mapping part. Used mainly to test all the feature before implementing it in the rover */
void callMapping();




#endif /* CALLMAPPING_H_ */
